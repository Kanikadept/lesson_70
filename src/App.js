import './App.css';
import Menu from "./components/Menu/Menu";

const App = () => (
    <div className="App">
        <Menu />
    </div>
);

export default App;

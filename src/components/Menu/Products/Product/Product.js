import React from 'react';
import './Product.css';
import {useDispatch, useSelector} from "react-redux";

const Product = (props) => {

    const dispatch = useDispatch();
    const response = useSelector(state => state.cart.products);


    return (
        <div className="product">
            <div className="image">
                <img src={props.image} alt=""/>
            </div>
            <div className="description">
                <div className="name">{props.name}</div>
                <div className="price">KGS {props.price}</div>
            </div>
            <button onClick={props.onClick}>Add to cart</button>
        </div>
    );
};

export default Product;
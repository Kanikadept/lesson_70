import React, {useEffect} from 'react';
import './Products.css';
import {useDispatch, useSelector} from "react-redux";
import {requestProducts} from "../../../store/actions/productActions";
import Product from "./Product/Product";
import {addProduct} from "../../../store/actions/cartAction";

const Products = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.product.products);

    useEffect(() => {
        dispatch(requestProducts());
    }, [dispatch]);


    const handleAddToCart = (product) => {
        dispatch(addProduct(product));
    }

    return ( products &&
        <div className="products">
            {Object.keys(products).map(e => (
                <Product onClick={() => handleAddToCart(products[e])} key={e} name={e} price={products[e].price} image={products[e].image}/>
            ))}
        </div>
    );
};

export default Products;
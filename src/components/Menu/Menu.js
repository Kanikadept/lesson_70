import React from 'react';
import './Menu.css';
import Products from "./Products/Products";
import Cart from "./Cart/Cart";

const Menu = () => {
    return (
        <div className="menu">
            <Products />
            <Cart />
        </div>
    );
};

export default Menu;
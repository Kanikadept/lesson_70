import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import './Cart.css';
import {changeTotal, removeProduct} from "../../../store/actions/cartAction";
import CartItem from "./CartItem/CartItem";

const Cart = () => {

    const dispatch = useDispatch();
    const cart = useSelector(state => state.cart.products)
    const totalPrice = useSelector(state => state.cart.totalPrice);
    const delivery = useSelector(state => state.cart.delivery);

    const handleRemove = (product) => {
        dispatch(removeProduct(product));
    }

    useEffect(() => {
        dispatch(changeTotal());
    }, [dispatch,cart])

    return (
        <div className="cart">
            <h6>Cart</h6>
            {cart.map((p , index) => (
                <CartItem key={index} product={p} handleRemove={() => handleRemove(p)}/>
            ))}
            <p>Delivery: {delivery}</p>
            <p>Total Price: {totalPrice}</p>
        </div>
    );
};

export default Cart;
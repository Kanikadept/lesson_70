import React from 'react';
import './CartItem.css';

const CartItem = props => {
    return (

        <div className="cart-item" >
            <p>{props.product.name} <span>{props.product.count}</span>
                <button onClick={props.handleRemove}>remove</button></p>
        </div>
    );
};

export default CartItem;
import {ADD_PRODUCT, CHANGE_TOTAL, REMOVE_PRODUCT} from "../actions/cartAction";

const initialState = {
    products: [],
    delivery: 150,
    totalPrice: 0
}

const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_PRODUCT:
            const products = [...state.products];
            const obj = action.product;
            const res = products.find(x => x.name === obj.name);
            if(res) {
                res.count += 1;
                return {...state, products: products}
            }
            action.product.count = 1;
            return {...state, products : state.products.concat(action.product)}
        case REMOVE_PRODUCT:
            const productsDel = [...state.products];
            const objDel = action.product;
            const resDel = productsDel.find(x => x.name === objDel.name);

            if(resDel) {
                if(resDel.count === 1) {
                    const result = productsDel.indexOf(resDel);
                    productsDel.splice(result, 1)
                    return {...state, products: productsDel}
                }
                resDel.count -= 1;

                return {...state, products: productsDel}
            }
            return state;
        case CHANGE_TOTAL:
            const total = state.products.reduce((acc, prod) => {

                return acc + prod.price * prod.count;
            }, state.delivery)
            return {...state, totalPrice: total}

        default:
            return state;
    }
}

export default cartReducer;
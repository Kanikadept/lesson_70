import {REQUEST_PRODUCTS_FAILURE, REQUEST_PRODUCTS_FETCH, REQUEST_PRODUCTS_SUCCESS} from "../actions/productActions";

const initialState = {
    products : null,
    loading: true,
}

const productReducer = (state = initialState, action) => {
    switch (action.type) {
        case REQUEST_PRODUCTS_FETCH:
            return {...state, loading: true};
        case REQUEST_PRODUCTS_SUCCESS:
            return {...state, products: action.products, loading: false};
        case REQUEST_PRODUCTS_FAILURE:
            return state;
        default:
            return state;
    }
}

export default productReducer;
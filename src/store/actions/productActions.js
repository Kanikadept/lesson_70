import axios from "axios";

export const REQUEST_PRODUCTS_FETCH = 'REQUEST_PRODUCTS';
export const requestProductsFetch = () => ({type: REQUEST_PRODUCTS_FETCH})

export const REQUEST_PRODUCTS_SUCCESS = 'REQUEST_PRODUCTS_SUCCESS';
export const requestProductsSuccess = products => ({type: REQUEST_PRODUCTS_SUCCESS, products})
export const requestProducts = () => {
    return async dispatch => {
        try {
            const productsResponse = await axios.get('https://lesson-70-kani-default-rtdb.firebaseio.com/products.json');
            dispatch(requestProductsSuccess(productsResponse.data));
        } catch (e) {
            dispatch();
        }
    }
}


export const REQUEST_PRODUCTS_FAILURE = 'REQUEST_PRODUCTS_FAILURE';
export const requestProductsFailure = () => ({type: REQUEST_PRODUCTS_FAILURE})
export const ADD_PRODUCT = 'ADD_PRODUCT';
export const addProduct = product => ({type: ADD_PRODUCT, product});

export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const removeProduct = product => ({type: REMOVE_PRODUCT, product});

export const CHANGE_TOTAL = 'CHANGE_TOTAL';
export const changeTotal = () => ({type: CHANGE_TOTAL});